const request = require('request');

const URL_BASE = "https://anapioficeandfire.com/api/";

module.exports = {
  /*
  ** This method returns a promise
  ** which gets resolved or rejected based
  ** on the result from the API
  */
  getAllCharacter : function(page, limit, query){      
    return new Promise((resolve, reject) => {
      request(URL_BASE.concat("characters?page=" + page + "&pageSize=" + limit + "&name=" + query), { json: true }, (err, res, body) => {                
        if (err) reject(err)
        resolve(body)
      });
    })
  },

  /*
  ** This method returns a promise
  ** which gets resolved or rejected based
  ** on the result from the API
  */
  getCharacterById : function(id){
    return new Promise((resolve, reject) => {
      request(URL_BASE.concat("characters/").concat(id), { json: true }, (err, res, body) => {        
        if (err) reject(err)
        resolve(body)
      });
    })
  }
}