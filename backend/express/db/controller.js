const pgp = require('pg-promise')();

// Database connection;
const cn = {
    host: 'localhost', // 'localhost' is the default;
    port: 5432, // 5432 is the default;
    database: 'prueba_seleccion',
    user: 'postgres',
    password: '123456'
};

const db = pgp(cn); 

module.exports = {
  select: async function (table, cols, condition) {
    try {
      const data = await db.any(
        'SELECT ' + (cols ? cols : '*') + ' ' +
        'FROM ' + table + ' ' +
        (condition ? condition : ''), 
        [true]);
      console.log(data);
      return data;
    } 
    catch(e) {
      console.log(e);
      return [];        
    }
  },
  exist: async function (table, condition) {
    try {
      const data = await db.any(
        'SELECT id ' +
        'FROM ' + table + ' ' +
        (condition ? condition : '') + ' LIMIT 1', 
        [true]);

      return data && data.length > 0;
    } 
    catch(e) {
      console.log(e);
      return false;        
    }
  },
  insert: function (table, query, data) {
    db.none('INSERT INTO ' + table + ' ' + query, data)
      .then(() => {
        console.log("Guardado exitoso");
      })
      .catch(error => {
        console.error("Error guardando datos", error);
      });
  },
  formatArray: function (data) {
    let value = '{';
    if (data.length > 0) {
      data.forEach((item, i) => {
        if (item) {
          value = value.concat('\'' + item + '\'');
          if (i < data.length - 1) {
            value = value.concat(', ');
          }
        }        
      });
    }    
    value = value.concat('}');    
    return value;
  }
}