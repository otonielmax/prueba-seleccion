const cols = [
    'url',
    'name',
    'gender',
    'culture',
    'born',
    'died',
    'titles',
    'aliases',
    'father',
    'mother',
    'spouse',
    'allegiances',
    'books',
    'povbooks',
    'tvseries',
    'playedby'
];

module.exports = {
  getAllCols: function () {
    let data = '';
    cols.forEach((item, i) => {
      data = data.concat(item);
      if (i < cols.length - 1) {
        data = data.concat(', ');
      }
    });
    return data;
  },
  getAllValuePosition: function () {
    let data = '';
    cols.forEach((item, i) => {
      data = data.concat('$' + (i + 1));
      if (i < cols.length - 1) {
        data = data.concat(', ');
      }
    });
    return data;
  }
}