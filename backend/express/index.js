// Imports
const express = require("express");
const api_oficeandfire = require('./services/api_oficeandfire');
const db = require('./db/controller');
const cors = require('cors')

// Utils 
const service_conf = require('./config/services');
const corsOptions = {
  origin: 'http://localhost:4200'
};

// Models
const character = require('./models/character');

// Constant
const PORT = 3000;

// Server Listen
const app = express();

// Endpoints
// Option Enabled
app.options('*', cors());
// Get character by id
app.get('/characters/:id', cors(corsOptions), function (req, res) {
  let id = req.params.id;
  api_oficeandfire.getCharacterById(id)
    .then(response => {
      if (response) {
        let cols = character.getAllCols();        
        let value = character.getAllValuePosition();
        if (!db.exist('characters', 'WHERE url = \'' + response.url + '\'')) {
          db.insert(
            'characters', 
            '(' + cols + ') ' +
            'VALUES (' + value + ')', 
            [response.url, response.name, response.gender, response.culture, response.born, response.died, db.formatArray(response.titles), db.formatArray(response.aliases), response.father, response.mother, response.spouse, db.formatArray(response.allegiances), db.formatArray(response.books), db.formatArray(response.povBooks), db.formatArray(response.tvSeries), db.formatArray(response.playedBy)]
          );
        }          
      }
      res.send(response);
    })
    .catch(error => {
      console.log("Error", error);
      res.send(error);
    });  
})
// Get all character
app.get('/characters', cors(corsOptions), function (req, res) {  
  api_oficeandfire.getAllCharacter(req.query.page ? req.query.page : 1, req.query.limit ? req.query.limit : service_conf.limit, req.query.name ? req.query.name : '')
    .then(response => {      
      if (response) {
        let cols = character.getAllCols();        
        let value = character.getAllValuePosition();
        response.forEach((item, i) => {
          if (!db.exist('characters', 'WHERE url = \'' + item.url + '\'')) {
            db.insert(
              'characters', 
              '(' + cols + ') ' +
              'VALUES (' + value + ')', 
              [item.url, item.name, item.gender, item.culture, item.born, item.died, db.formatArray(item.titles), db.formatArray(item.aliases), item.father, item.mother, item.spouse, db.formatArray(item.allegiances), db.formatArray(item.books), db.formatArray(item.povBooks), db.formatArray(item.tvSeries), db.formatArray(item.playedBy)]
            );
          }          
        });          
      }
      res.json(response);
    })
    .catch(error => {
      console.log("Error", error);
      res.send(error);
    });  
})

// Server Listen Start
app.listen(PORT, () => {
  console.log("Servidor activo en puerto ".concat(PORT));
});