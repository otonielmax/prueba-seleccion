import { ApiService } from './../../../../services/api/api.service';
import { Component, OnInit } from '@angular/core';
import { ServiceConfigDirective } from 'src/services/service-config.directive';
import { ActivatedRoute, Router } from '@angular/router';

const ROUTE = '/character';

@Component({
  selector: 'app-character-details',
  templateUrl: './character-details.component.html',
  styleUrls: ['./character-details.component.css']
})
export class CharacterDetailsComponent implements OnInit {

  public data;
  public id: string;

  constructor(
    private _apiService: ApiService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.checkRoute();
  }

  public checkRoute() {
    this._activatedRoute.paramMap.subscribe(
      response => {     
        if (response.get('id')) {
          this.id = response.get('id');
          this.getCharacterById();
        }              
      }
    );
  }

  public getCharacterById() {
    this._apiService.getCharacterById(this.id).subscribe(
      response => {
        if (ServiceConfigDirective.validResponse(response)) {          
          this.data = response;
        }
    })
  }

  public goBack() {    
    this._router.navigate([ROUTE]);
  }

}
