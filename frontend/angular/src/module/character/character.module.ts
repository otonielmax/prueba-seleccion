import { CharacterRoutingModule } from './character-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManagmentCharacter } from './managment/managment.character';
import { CharacterDetailsComponent } from './components/character-details/character-details.component';

@NgModule({
  declarations: [ManagmentCharacter, CharacterDetailsComponent],
  imports: [
    CommonModule,
    CharacterRoutingModule
  ]
})
export class CharacterModule { }
