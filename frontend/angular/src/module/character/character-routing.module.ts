import { ManagmentCharacter } from './managment/managment.character';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { 
    path: '',
    component: ManagmentCharacter,    
  },
  { 
    path: ':id',
    component: ManagmentCharacter,    
  },
  {
    path: '**',
    redirectTo: '/error/404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CharacterRoutingModule { }
