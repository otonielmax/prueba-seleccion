import { ApiService } from './../../../services/api/api.service';
import { Component, OnInit } from '@angular/core';
import { ServiceConfigDirective } from 'src/services/service-config.directive';
import { ActivatedRoute, Router } from '@angular/router';

const ROUTE = '/character';

@Component({
  selector: 'management-character',
  templateUrl: './managment.character.html'
})
export class ManagmentCharacter implements OnInit {

  public data: Array<any> = [];
  public action = null;
  public filter: any = {
    page: 1,
    pageSize: 10,
    name: ''
  };
  public query: string;
  public isUpdate: boolean = false;
  public timeOut: any = null;

  constructor(
    private _apiService: ApiService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,    
    ) {                        
  }

  ngOnInit() {
    this.checkRoute();
  }

  public checkRoute() {
    this._activatedRoute.paramMap.subscribe(
      response => {     
        if (response.get('id')) {
          this.action = response.get('id');
        }
        else {
          this.action = null;
          this.getCharacters();
        }                
      }
    );
  }

  public getCharacters() {
    this.isUpdate = true;
    this._apiService.getCharacters(this.filter).subscribe(
      response => {
        if (ServiceConfigDirective.validResponse(response)) {          
          this.data = response;
        }
        this.isUpdate = false;
    })
  }

  public goDetail(item) {    
    let separator = item.url.split('/');
    let id = separator[separator.length - 1];
    this._router.navigate([ROUTE, id]);
  }

  public prev() {
    if (this.filter.page > 1) {
      this.filter.page--;
      this.getCharacters();
    }    
  }

  public next() {
    this.filter.page++;
    this.getCharacters();
  }

  public filterByQuery(val) {    
    console.log(val);
    this.filter.name = val.target.value;
    clearTimeout(this.timeOut);
    this.timeOut = setTimeout(() => {
      this.getCharacters();
    }, 1000);
  }

  ngOnDestroy() {    
  }
  
}
