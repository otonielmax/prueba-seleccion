import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: '',
    redirectTo: 'character',
    pathMatch: 'full'
  },
  { path: 'character',
    loadChildren: './character/character.module#CharacterModule',    
  },
  {
    path: '**',
    redirectTo: '/error/404',
    pathMatch: 'full'
  }
];
console.log("Importado");

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModuleRoutingModule { }
