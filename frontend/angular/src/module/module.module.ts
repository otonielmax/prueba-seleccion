import { NgModule } from '@angular/core';

import { ModuleRoutingModule } from './module-routing.module';


@NgModule({
  declarations: [],
  imports: [
    ModuleRoutingModule
  ]
})
export class ModuleModule { }
