import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ServiceConfigDirective } from '../service-config.directive';

const ENDPOINT = 'characters';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private _http: HttpClient) { 

  }

  /**
   * @name getCharacters
   * @description service that allows you to get all characters
   */
  public getCharacters(params?: any): Observable<any> {
    return this._http
      .get(environment.baseUrl.concat(ENDPOINT), ServiceConfigDirective.options(params))
      .pipe(
        catchError(err => {
          return ServiceConfigDirective.handleError(err);
        })
      );
  }

  /**
   * @name getCharacterById
   * @description service that allows you to get character by id
   */
  public getCharacterById(id: string, params?: any): Observable<any> {
    return this._http
      .get(environment.baseUrl.concat(ENDPOINT).concat('/').concat(id), ServiceConfigDirective.options(params))
      .pipe(
        catchError(err => {
          return ServiceConfigDirective.handleError(err);
        })
      );
  }
}
