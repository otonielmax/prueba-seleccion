import { Directive } from '@angular/core';
import { throwError } from 'rxjs';
import { HttpHeaders, HttpErrorResponse, HttpParams, HttpResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Directive({
  selector: '[ServiceConfig]'
})
export class ServiceConfigDirective {
  
  /**
   * @name handleError
   * @description notifies to the screen the error message, if it has no message, it shows a default message.
   * @param error http error to proccess.
   * @param _miscellaneousService reference to the service that allows notification and loading management.
   */
  static handleError(error: HttpErrorResponse) {
    console.log(error);
    return throwError(error.error);
  }

  /**
   * @name validResponse
   * @description validates if the response is ok and notify some message if there is one different than 'ok'.
   * @param response http response to proccess.
   * @param _miscellaneousService reference to the service that allows notification and loading management.
   */
  static validResponse(response: any){
    console.log(response);        
    return !response.error;
  }

  /**
   * @name validResponseNotData
   * @description validates if the response is ok and notify some message if there is one different than 'ok'.
   * @param response http response to proccess.
   * @param _miscellaneousService reference to the service that allows notification and loading management.
   */
  static validResponseNotData(response: any){
    console.log(response);
    return !response.error;
  }

  /**
   * @name headers
   * @description returns the http headers for all the requests (content type and authorization bearer).
   */
  static get headers(): HttpHeaders {    
    const headers = new HttpHeaders({
      'Content-Type': environment.contentType
    });
      return headers;
  }

  /**
   * @name validResponse
   * @description returns the options of the request.
   * @param params if exists, params to search on a get request.
   */
  static options(params?: any): { headers: HttpHeaders, params?: HttpParams } {
    return { headers: this.headers, params: params }
  }
}
